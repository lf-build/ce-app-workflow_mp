const schema = {
};

function* set({
  $api,
  $set,
  $get,
  $facts: {
    opportunity,
    qualify,
  },
  $debug,
  $configuration
}) {

  $debug('submit lead');

  const {
    'application-processor': applicationProcessorEndpoint,
    'data-attribute': dataAttributeEndPoint
  } = yield $configuration('endpoints');

  const {
    amount,
    reason: {
      reason,
      reasonText
    },
    basicInformation: {
      title,
      maritalStatus,
      firstName,
      middleName,
      lastName,
      personalEmail
    },
  } = yield opportunity.$get();

  // This will not cause ...
  let panNumber = undefined;
  try {
    const qualifyState = yield qualify.$get();

    if (qualifyState && qualifyState.details) {
      panNumber = qualifyState.details.panNumber;
    }
    $debug('Pan-Number detected and will be part of partial application');
  } catch (e) {
    $debug('in catch, error occurred ', e);

  }

  const {
    user: {
      id: userId,
    },
    mobile: {
      mobileNumber,
      verification: {
        status: mobileVerificationStatus,
        date: mobileVerificationDate
      }
    },
    application: {
      applicationNumber,
    }
  } = yield $get();

  const applicationSubmitPayload = {
    purposeOfLoan: reason,
    OtherPurposeDescription: reasonText,
    requestedAmount: amount,
    personalMobile: mobileNumber,
    isMobileVerified: mobileVerificationStatus,
    mobileVerificationTime: mobileVerificationDate,
    firstName: firstName,
    middleName: middleName,
    lastName: lastName,
    personalEmail,
    userId: userId,

    // CE-1131: BorrowerPortal: Need to set few parameters values at the time of lead creation.
    gender: title === 'Mr'
      ? 'Male'
      : 'Female',
    salutation: title,
    maritalStatus: maritalStatus,
    sourceReferenceId: 'Organic',
    sourceType: 'Organic',
    systemChannel: 'BorrowerPortal',
    trackingCode: 'BorrowerPortal',
    paymentFrequency: 'Monthly',
    requestedTermType: 'Monthly',
    requestedTermValue: '12.5',
    employmentStatus: 'Salaried'
  };

  if (panNumber) {
    applicationSubmitPayload.permanentAccountNumber = panNumber;
  } else {
    // we will be calling data-attribute to get the PAN number so that if application is processed
    // from back-office re-apply will work.
    try {
      const response = yield $api.get(`${dataAttributeEndPoint}/application/${applicationNumber}/application`);      
      if (response && response.body) {
        const {
          pan
        } = response.body;

        applicationSubmitPayload.permanentAccountNumber = pan;
        $debug(`Pan-Number fetched from data-attributes and it is ${pan}`);
      }
    } catch (e) {
      $debug('in catch, some error occurred while calling submit-lead ', e);
      throw {
        code: 424,
        message: e,
      }
    }
  }

  const appResponse = yield $api.post(`${applicationProcessorEndpoint}/application/submit`, applicationSubmitPayload);
  const parsedApplicationObject = Object.assign(applicationSubmitPayload, {
    applicationNumber: appResponse.body.applicationNumber,
    applicantId: appResponse.body.applicantId
  });
  yield $set('application', parsedApplicationObject);

  return parsedApplicationObject;
};

module.exports = [schema, set];
