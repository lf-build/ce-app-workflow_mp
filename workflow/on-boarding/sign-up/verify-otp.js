const schema = {
  otp: 'string: min=4,max=4,required',
  utmSource: 'string: min=1,max=250',
  utmMedium: 'string: min=1,max=250',
  gclId: 'string: min=1,max=250',
};

function* set({
  $api,
  $get,
  $set,
  $setStatus,
  $configuration,
  value: {
    otp,
    utmSource,
    utmMedium,
    gclId,
  },
  $debug,
  $facts: {
    opportunity
  },
  $stages: {
    authorization: {
      identity: {
        'create-account': createAccount,
        login
      }
    }
  },
}) {
  const { otp: otpEndpoint } = yield $configuration('endpoints');
  const { borrower: { en } } = yield $configuration('i18n');
  const { 'api.on-boarding.sign-up.verify-otp.expired': expiredMessage,
    'api.on-boarding.sign-up.verify-otp.other': otherMessage } = en;
  const { 'otp-verification': refNo, verifyAttempts } = yield $get();

  if (process.env.OTP_MODE && process.env.OTP_MODE === 'LIVE') {
    if (verifyAttempts >= 3) {
      throw {
        code: 422,
        message: 'OTP expired or did not match',
        maxAttemptsReached: true
      }
    }
  }

  try {
    if (process.env.OTP_MODE && process.env.OTP_MODE === 'LIVE') {
      $set('verifyAttempts', verifyAttempts ? verifyAttempts + 1 : 1);
      const otpResponse = yield $api.post(`${otpEndpoint}/verify/${refNo}/${otp}`, {});
    }

    // setting the date in workflow when mobile number got verified.
    yield $set('mobile', 'verification', {
      date: (new Date())
        .toISOString()
        .substring(0, 10),
      status: true
    });
  } catch (error) {
    let messageToDisplay = "";
    const messageSplit = error.body.message.split(":");
    let finalMessage = messageSplit[(messageSplit.length - 1)];
    console.log(finalMessage);
    console.log(otherMessage);
    if (finalMessage.trim() === 'otp_expired') {
      messageToDisplay = expiredMessage;
    }
    else {
      messageToDisplay = otherMessage;
    }

    throw {
      code: 422,
      "details": [
        {
          "path": "otp",
          message: messageToDisplay
        }
      ]
    }
  }

  // creating account and login
  $debug('now executing the logic to create-account, login and submit application');
  try {
    // as create account page is skipped need to create account after OTP is entered
    const randomPassword = Math.floor(100000 + Math.random() * 900000);
    $debug(`random number is ${randomPassword}`);
    const { mobileNumber } = yield $get('mobile');

    const { 'application-processor': applicationProcessorEndpoint } = yield $configuration('endpoints');
    const {
      amount,
      reason: {
        reason,
        reasonText
      },
      basicInformation: {
        title,
        maritalStatus,
        firstName,
        middleName,
        lastName,
        personalEmail
      }
    } = yield opportunity.$get();

    const {
      mobile: {
        verification: {
          status: mobileVerificationStatus,
          date: mobileVerificationDate
        }
      }
    } = yield $get();

    const newUser = yield createAccount.$execute({ username: mobileNumber, password: randomPassword.toString(), name: `${firstName} ${lastName}`, email: personalEmail });
    yield $set('user', Object.assign({}, newUser, { randomNumber: randomPassword }));

    const loginDetails = yield login.$execute({ username: mobileNumber, password: randomPassword.toString() });

    const applicationSubmitPayload = {
      purposeOfLoan: reason,
      OtherPurposeDescription: reasonText,
      requestedAmount: amount,
      personalMobile: mobileNumber,
      isMobileVerified: mobileVerificationStatus,
      mobileVerificationTime: mobileVerificationDate,
      firstName: firstName,
      middleName: middleName,
      lastName: lastName,
      personalEmail,
      userId: newUser.id,

      // CE-1131: BorrowerPortal: Need to set few parameters values at the time of lead creation.
      gender: title === 'Mr'
        ? 'Male'
        : 'Female',
      salutation: title,
      maritalStatus: maritalStatus,
      sourceReferenceId: 'Organic',
      sourceType: 'Organic',
      systemChannel: 'BorrowerPortal',
      trackingCode: utmSource || 'BorrowerPortal',
      trackingCodeMedium: utmMedium || 'BorrowerPortal',
      GCLId: gclId || 'BorrowerPortal',
      paymentFrequency: 'Monthly',
      requestedTermType: 'Monthly',
      requestedTermValue: '12.5',
      employmentStatus: 'Salaried'
    };
    const appResponse = yield $api.post(`${applicationProcessorEndpoint}/application/submit`, applicationSubmitPayload);
    const parsedApplicationObject = Object.assign(applicationSubmitPayload, {
      applicationNumber: appResponse.body.applicationNumber,
      applicantId: appResponse.body.applicantId,
      productId: appResponse.body.productId,
    });workflow/on-boarding/state/restore.js
    yield $set('application', parsedApplicationObject);

    return Object.assign(loginDetails, { application: parsedApplicationObject });
  } catch (error) {
    throw {
      code: 422,
      error
    }
  }
};

module.exports = [schema, set];
