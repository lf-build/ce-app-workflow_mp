const { dbAction } = require('@sigma-infosolutions/orbit-base/storage');

const schema = {
    mobileNumber: {
        '@type': 'string',
        'regex': /^[6-9][0-9]{9}$/,
        required: true
    }
};

function* set({
    $api,
    $set,
    $configuration,
    value: {
        mobileNumber
    },
    $debug
  }) {
    const {
      'application-processor': applicationProcessorEndpoint
    } = yield $configuration('endpoints');

    try {
        // getting data from db based on mobileNumber.
        // There can be a chance when the workflow db's store will have more than 1 entry with same mobile number
        // so to handle such cases we have added .sort({ _id: -1 }) here -1 will sort in decending order i.e. latest 
        // entry will come first
        const readAllStates = (db) => db
            .collection('store')
            .find({ 'on-boarding.sign-up.signupStatus.username': mobileNumber })
            .sort({ _id: -1 })
            .toArray();
        let states = yield dbAction(readAllStates);

        if (states.length === 0) {
            throw { code: 404, message: `mobileNumber: ${mobileNumber} to state mapping not found` }
        }

        let [persistedState] = states;
        $debug('Workflow persistedState._id ', persistedState._id);

        if (persistedState['on-boarding'].application || persistedState['on-boarding']['sign-up'].application) {
            const {
                    type,
                    application: {
                        applicationNumber,
                        applicantId
                    }
            } = persistedState['on-boarding']['sign-up'] || persistedState['on-boarding'].application;

            $debug(`Application Number - ${applicationNumber} and Applicant Id - ${applicantId}`);
            const appResponse = yield $api.post(`${applicationProcessorEndpoint}/application/${applicationNumber}/${applicantId}/EnableReApplyInCoolOff`);
            console.log('appResponse ', appResponse);
            return appResponse;
        }
    } catch (e) {
        throw {
            code: 422,
            message: e
        };
    }
};

module.exports = [schema, set];
