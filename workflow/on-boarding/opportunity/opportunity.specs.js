const chai = require('chai');
chai.use(require('chai-as-promised'));
const {expect} = chai;

describe('Opportunity', () => {
  describe('set-amount', () => {
    const moduleToTest = require('./set-amount');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });

  describe('set-basic-information', () => {
    const moduleToTest = require('./set-basic-information');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });

  describe('set-reason', () => {
    const moduleToTest = require('./set-reason');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
});
