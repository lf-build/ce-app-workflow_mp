const schema = {
  'reason': {
    '@type': 'string',
    valid: ['travel', 'vehiclepurchase', 'medical', 'loanrefinancing', 'wedding', 'homeimprovement', 'business', 'education', 'assetacquisition', 'agriculture', 'other'],
    required: true
  },
  'reasonText': 'string: min=2, max=100'
};

function *set({$set, $get, value})  {
  return yield $set('reason', value);
};

module.exports = [schema, set];
