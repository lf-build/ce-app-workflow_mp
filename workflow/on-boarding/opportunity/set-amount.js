const schema = {
  amount: 'number:min=25000,max=1500000,required'
};

function* set({
  $set,
  value: {
    amount
  }
}) {
  return yield $set('amount', amount);
};

module.exports = [schema, set];