const schema = {};

function* set({
    $api,
    $configuration,
    $debug,
}) {
    const { configuration: configurationEndpoint } = yield $configuration('endpoints');

    try {
        const logoReponse = yield $api.get(`${configurationEndpoint}/borrower-portal`);
        return logoReponse && logoReponse.body;
    } catch (error) {
        throw {
            error
        };
    }
};

module.exports = [schema, set];
