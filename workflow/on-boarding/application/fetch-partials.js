const { dbAction } = require('@sigma-infosolutions/orbit-base/storage');
function customISOstring(date, offset) {
  var date = new Date(date), // copy instance
    h = Math.floor(Math.abs(offset) / 60),
    m = Math.abs(offset) % 60;
  date.setMinutes(date.getMinutes() - offset); // apply custom timezone
  function pad(n) { return n < 10 ? '0' + n : n }
  return date.getUTCFullYear() + '-' // return custom format
    + pad(date.getUTCMonth() + 1) + '-'
    + pad(date.getUTCDate()) + 'T'
    + pad(date.getUTCHours()) + ':'
    + pad(date.getUTCMinutes()) + ':'
    + pad(date.getUTCSeconds())
    + (offset == 0 ? "Z" : (offset < 0 ? "+" : "-") + pad(h) + ":" + pad(m));
}

const flatten = (app) => {
  const {
    opportunity,
    'sign-up': signup,
    qualify,
  } = app['on-boarding'];

  let flatApplication = {
    "amountRequested": opportunity.amount,
    "requestTermType": "Monthly",
    "requestTermValue": 12.5,
    "applicantFirstName": opportunity.basicInformation.firstName,
    "applicantLastName": opportunity.basicInformation.lastName,
    "applicantMiddleName": opportunity.basicInformation.middleName,
    "applicantPersonalEmail": opportunity.basicInformation.personalEmail,
    "applicantPhone": signup.mobile.mobileNumber,
    "applicantAddressCountry": 'India',
    "applicantAddressIsDefault": true,
    "sourceId": "Organic",
    "sourceType": "Organic",
    "workflowId": app._id,
  };

  if (qualify) {
    if (qualify.work) {
      Object.assign(flatApplication, {
        "applicantEmployer": qualify.work.employer.name,
        "applicantWorkEmail": qualify.work.officialEmail,
      });
    }
    if (qualify.details) {
      Object.assign(flatApplication, {
        "applicantDateOfBirth": customISOstring(qualify.details.dateOfBirth, (new Date(qualify.details.dateOfBirth)).getTimezoneOffset()),
        "applicantPanNumber": qualify.details.panNumber,
        "currentAddressYear": qualify.details.currentAddressYear,
        "applicantAadharNumber": qualify.details.aadhaarNumber,
        "applicantAddressLine1": qualify.details.currentResidentialAddress.addressLine1,
        "applicantAddressLine2": qualify.details.currentResidentialAddress.addressLine2,
        "applicantAddressLine3": qualify.details.currentResidentialAddress.locality,
        "applicantAddressLine4": undefined,
        "applicantAddressCity": qualify.details.currentResidentialAddress.city,
      });
    }
  }

  return flatApplication;
}
const schema = {
  page: 'number:min=1,max=500000,required',
  pageSize: 'number:min=1,max=500000,required'
};

function* set({
  $api,
  value: {
    page,
    pageSize
  },
  $configuration,
  $debug
}) {

  function* readAllApplications(db) {
    const q = db
      .collection('store')
      .find({
        'on-boarding.application': {
          $exists: false
        },
        'on-boarding.sign-up': {
          $exists: true
        }
      })
      .sort({ _id: -1 })
      .skip((page - 1) * pageSize)
      .limit(pageSize);

    const total = yield q.count();

    return {
      page,
      pageSize,
      total,
      totalPages: Math.ceil((total / pageSize)),
      applications: (yield q.toArray()).map(flatten)
    };
  }

  return yield dbAction(readAllApplications);
};

module.exports = [schema, set];
