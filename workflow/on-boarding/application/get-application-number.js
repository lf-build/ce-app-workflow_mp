const schema = {};

function* set({
    $api,
    $set,
    $facts: {
        'sign-up': signUp
    },
    value,
    $debug
}) {

    try {
        const {
            application: {
                applicationNumber
            }
        } = yield signUp.$get();

        return applicationNumber
    } catch (e) {
        $debug(e);
        throw {
            code: 422,
            message: e
        }
    }
};

module.exports = [schema, set];