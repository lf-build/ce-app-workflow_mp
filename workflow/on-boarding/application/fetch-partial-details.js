const { dbAction } = require('@sigma-infosolutions/orbit-base/storage');
function customISOstring(date, offset) {
  var date = new Date(date);
  function pad(n) { return n < 10 ? '0' + n : n }
  return date.getUTCFullYear() + '-' // return custom format
    + pad(date.getUTCMonth() + 1) + '-'
    + pad(date.getUTCDate())
}

const flatten = (app) => {
  const {
    opportunity,
    'sign-up': signup,
    qualify,
  } = app['on-boarding'];

  let flatApplication = {
    "workflowId": app._id,
    "requestedAmount": opportunity.amount,
    "requestedTermType": "Monthly",
    "requestedTermValue": 12.5,
    "purposeOfLoan": opportunity.reason.reason,
    "firstName": opportunity.basicInformation.firstName,
    "lastName": opportunity.basicInformation.lastName,
    "middleName": opportunity.basicInformation.middleName,
    "gender": opportunity.basicInformation.title === 'Mr' ? 'Male' : 'Female',
    "maritalStatus": opportunity.basicInformation.maritalStatus,
    "salutation": opportunity.basicInformation.title,
    "personalEmail": opportunity.personalEmail,
    "mobileNo": signup.mobile.mobileNumber,
    "applicantAddressCountry": 'India',
    "applicantAddressIsDefault": true,
    "source": {
      "TrackingCode": "TTMC",
      "SystemChannel": "Organic",
      "SourceType": "Organic",
      "SourceReferenceId": null
    },
  };

  if (qualify) {
    if (qualify.work) {
      Object.assign(flatApplication, {
        "cinNumber": qualify.work.employer.cin,
        "applicantEmployer": qualify.work.employer.name,
        "companyEmailAddress": qualify.work.officialEmail,
        "employeeType": "Salaried",
        "income": qualify.work.monthlyTakeHomeSalary,
      });
    }
    if (qualify.residenceExpenses) {
      Object.assign(flatApplication, {
        "selfEmi": qualify.residenceExpenses.homeLoanEmi,
        "residenceType": qualify.residenceExpenses.residenceType,
        "monthlyRent": qualify.residenceExpenses.monthlyRent,
      });
    }
    if (qualify.creditCardExpenses) {
      Object.assign(flatApplication, {
        "creditCardBalance": qualify.creditCardExpenses.creditCardOutstanding,
      });
    }
    if (qualify.otherExpenses) {
      Object.assign(flatApplication, {
        "selfEmi": flatApplication.selfEmi + qualify.otherExpenses.otherEmis,
      });
    }
    if (qualify.details) {
      Object.assign(flatApplication, {
        "dateOfBirth": customISOstring(qualify.details.dateOfBirth),
        "pan": qualify.details.panNumber,
        "aadharNumber": qualify.details.aadhaarNumber,
        "currentAddressYear": qualify.details.currentAddressYear,
        "city": qualify.details.currentResidentialAddress.city.state,
        "zipCode": qualify.details.currentResidentialAddress.city.pinCode,
        "currentAddress": {
          "AddressLine1": qualify.details.currentResidentialAddress.addressLine1,
          "AddressLine2": qualify.details.currentResidentialAddress.addressLine2,
          "AddressLine3": undefined,
          "AddressLine4": undefined,
          "LandMark": null,
          "Location": qualify.details.currentResidentialAddress.locality,
          "City": qualify.details.currentResidentialAddress.city,
          "State": qualify.details.currentResidentialAddress.city.state,
          "PinCode": qualify.details.currentResidentialAddress.city.pinCode,
          "Country": "India",
          "AddressType": "Current",
        }
      });
      if (qualify.details.currentResidentialAddress.permanentResidentialAddressSameAsCurrent) {
        Object.assign(flatApplication, {
          "permanentAddress": {
            "AddressLine1": qualify.details.currentResidentialAddress.addressLine1,
            "AddressLine2": qualify.details.currentResidentialAddress.addressLine2,
            "AddressLine3": undefined,
            "AddressLine4": undefined,
            "LandMark": null,
            "Location": qualify.details.currentResidentialAddress.locality,
            "City": qualify.details.currentResidentialAddress.city,
            "State": qualify.details.currentResidentialAddress.city.state,
            "PinCode": qualify.details.currentResidentialAddress.city.pinCode,
            "Country": "India",
            "AddressType": "Permanent",
          }
        });
      } else {
        Object.assign(flatApplication, {
          "permanentAddress": {
            "AddressLine1": qualify.details.permanentResidentialAddress.addressLine1,
            "AddressLine2": qualify.details.permanentResidentialAddress.addressLine2,
            "AddressLine3": undefined,
            "AddressLine4": undefined,
            "LandMark": null,
            "Location": qualify.details.permanentResidentialAddress.locality,
            "City": qualify.details.permanentResidentialAddress.city,
            "State": qualify.details.permanentResidentialAddress.city.state,
            "PinCode": qualify.details.permanentResidentialAddress.city.pinCode,
            "Country": "India",
            "AddressType": "Permanent",
          }
        });

      }

    }
  }


  //     "application": {
  //       "creditCardBalance": 1500,
  //       "monthlyExpense": 7500,
  //     };

  return flatApplication;
}
const schema = {
  workflowId: 'number:min=1,max=9489570656360,required',
};

function* set({
  $api,
  value: {
    workflowId
  },
  $configuration,
  $debug
}) {

  function* readAllApplications(db) {
    const q = db
      .collection('store')
      .find({
        _id: workflowId.toString()
      });

    return (yield q.toArray()).map(flatten)[0];
  }

  return yield dbAction(readAllApplications);
};

module.exports = [schema, set];
