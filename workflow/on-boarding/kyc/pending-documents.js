const schema = {};

function* set({
  $set,
  $api,
  $facts: {
    'sign-up': signUp,
  },
  $configuration
}) {
  const {
    'verification-engine': verificationEngineEndPoint,
  } = yield $configuration('endpoints');

  const {
    application: {
      applicationNumber
    }
  } = yield signUp.$get();
  const pendingDocsResponse = yield $api.get(`${verificationEngineEndPoint}/application/${applicationNumber}/pending-document`);

  return pendingDocsResponse.body;
};

module.exports = [schema, set];
