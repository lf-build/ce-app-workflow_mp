const schema = {};

function* set({
    $api,
    $set,
    $setStatus,
    $configuration,
    $debug,
    $facts: {
        'sign-up': signUp,
    } }) {
    const { perfios: perfiosEndpoint } = yield $configuration('endpoints');
    const {
        application: {
            applicationNumber
        }
    } = yield signUp.$get();

    let status = undefined;

    try {
        status = yield $api.get(`${perfiosEndpoint}/application/${applicationNumber}/status`);
        $debug('Net Banking transaction status is : ', status);
    } catch (e) {
        $debug(e);
        throw {
            code: '422',
            message: 'error occurred while fetching net banking transaction status'
        };
    }

    return status;
};

module.exports = [schema, set];
