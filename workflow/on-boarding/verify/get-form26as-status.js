const schema = {};

function *set({
    $api,
    $set,
    $setStatus,
    $configuration,
    $debug,
    $facts: {
        application,
    }, }) {

  const { perfios: perfiosEndpoint, } = yield $configuration('endpoints');
  const { application: { applicationNumber } } = yield application.$get();

  let result = undefined;

  try {
    result = yield $api.get(`${perfiosEndpoint}/application/${applicationNumber}/form26as-status`);
    $debug('Form 26AS available ', result);
  } catch(e) {
    throw {
        code: 422,
        message: e
    }
  }
  
  return result;
};

module.exports = [schema, set];
