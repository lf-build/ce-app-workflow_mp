const schema = {
  searchText: 'string:min=0,max=200,required'
};

function* set({
  $api,
  value: {
    searchText,
  },
  $facts: {
    application,
  },
  $debug,
  $configuration
}) {
  const {
    'company-db': companydbEnpoint,
  } = yield $configuration('endpoints');

  const {
    application: {
      applicationNumber
    }
  } = {
    application: {
      applicationNumber: '000015'
    }
  }; //yield application.$get();
  try {
    // const lookupResponse = yield $api.get(`${companydbEnpoint}/application/${applicationNumber}/company/search/byname/${searchText}`);
    // return lookupResponse.body && lookupResponse.body.companySearchResponse && (lookupResponse.body.companySearchResponse.length ? lookupResponse.body.companySearchResponse : [{
    //   cin: searchText.toUpperCase(),
    //   name: searchText.toUpperCase()
    // }]);

    const autoCompleteApiResponse = yield $api.get(`https://suggest.autocompleteapi.com/apcdq66wnpce/employeedb?size=20&prefix=${searchText}`);
    const res = autoCompleteApiResponse.body.suggestions.map((company) => ({
      cin: company.value,
      name: company.value
    }));
    
    console.log('autoCompleteApiResponse.suggestions ', res);
    
    return res && (res.length > 0 ? res : [{
        cin: searchText.toUpperCase(),
        name: searchText.toUpperCase()
      }]);

  } catch (error) {
    throw {
      code: 424,
      error
    };
  }
};

module.exports = [schema, set];