const chai = require('chai');
chai.use(require('chai-as-promised'));
const {expect} = chai;

describe('bank-details', () => {
  describe('generate-final', () => {
    const moduleToTest = require('./bank-details');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });

  describe('pincode-details', () => {
    const moduleToTest = require('./pincode-details');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });

  describe('company', () => {
    const moduleToTest = require('./company');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
});
