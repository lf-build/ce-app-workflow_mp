const chai = require('chai');
chai.use(require('chai-as-promised'));
const {expect} = chai;

describe('Qualify', () => {
  describe('set-credit-card-expenses', () => {
    const moduleToTest = require('./set-credit-card-expenses');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('set-details', () => {
    const moduleToTest = require('./set-details');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('set-other-expenses', () => {
    const moduleToTest = require('./set-other-expenses');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('set-residence-expenses', () => {
    const moduleToTest = require('./set-residence-expenses');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
  describe('set-work-details', () => {
    const moduleToTest = require('./set-work-details');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
});
