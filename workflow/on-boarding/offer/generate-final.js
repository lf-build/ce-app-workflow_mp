const schema = {};

function* set({
  $set,
  $get,
  $api,
  $facts: {
    // application,
    'sign-up': signUp
  },
  $configuration
}) {
  const {
    'application-processor': applicationProcessorEndPoint,
  } = yield $configuration('endpoints');

  const {
    mobile: {
      mobileNumber
    },
    application: {
      applicationNumber
    }
  } = yield signUp.$get();

  let offerResponse = undefined;

  try {
    offerResponse = yield $api.get(`${applicationProcessorEndPoint}/offer/${applicationNumber}/finaloffer/presented`);

    // in case when status is 204, then we need to make post call to generate final-offer
    if (offerResponse) {
      const { status } = offerResponse;
      if (status && status.code === 204) {
        offerResponse = yield $api.post(`${applicationProcessorEndPoint}/offer/${applicationNumber}/finaloffer`);
      }
    }

  } catch (e) {
    offerResponse = yield $api.post(`${applicationProcessorEndPoint}/offer/${applicationNumber}/finaloffer`);
  }
  if (!offerResponse.body.finalOffers && offerResponse.body.status === 'Failed') {
    return ({
      status: 'failed'
    });
  } else if (!offerResponse.body.finalOffers) {
    return ({
      status: 'rejected'
    });
  } else {
    $set('final', offerResponse.body)
    return offerResponse.body;
  }

  $set('final', offer)

  return offer;
};

module.exports = [schema, set];