const chai = require('chai');
chai.use(require('chai-as-promised'));
const {expect} = chai;

describe('Offer', () => {
  describe('generate-final', () => {
    const moduleToTest = require('./generate-final');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });

  describe('generate-initial', () => {
    const moduleToTest = require('./generate-initial');
    it('Should have schema attached', () => {
      expect(moduleToTest instanceof Array).to.be.true;
      expect(moduleToTest[0]).to.be.an.object;
    });
  });
});
