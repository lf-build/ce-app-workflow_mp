const schema = {
    'applicationNumber': 'string: min=7, max=7, required',
};

function* set({
    $set,
    $get,
    $api,
    $debug,
    value: {
        applicationNumber
    },
    $configuration
}) {
    const {
        'application-processor': applicationProcessorEndPoint,
    } = yield $configuration('endpoints');

    let offerResponse = undefined;

    try {
        offerResponse = yield $api.post(`${applicationProcessorEndPoint}/offer/${applicationNumber}/finaloffer`);

        if (!offerResponse.body.finalOffers && offerResponse.body.status === 'Failed') {
            return ({
                status: 'failed'
            });
        } else if (!offerResponse.body.finalOffers) {
            return ({
                status: 'rejected'
            });
        } else {
            return offerResponse.body;
        }
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];