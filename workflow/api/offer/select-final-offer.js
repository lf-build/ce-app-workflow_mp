const schema = {
    'selectedOfferId': 'string:min=2,max=200,required',
    'applicationNumber': 'string: min=7, max=7, required',
};

function* set({
    $api,
    $set,
    value: {
        applicationNumber,
        selectedOfferId
    },
    $debug,
    $configuration
}) {
    const {
        'application-processor': applicationProcessorEndpoint,
    } = yield $configuration('endpoints');

    try {
        const offerResponse = yield $api.put(`${applicationProcessorEndpoint}/offer/${applicationNumber}/acceptfinaloffer/${selectedOfferId}`);

        return {
            selectedOfferId
        };
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];