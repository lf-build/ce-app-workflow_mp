const schema = {
    'requestedAmount': 'number:min=50000,max=750000,required',
    'purposeOfLoan': {
        '@type': 'string',
        valid: ['travel', 'vehiclepurchase', 'medical', 'loanrefinancing', 'wedding', 'homeimprovement', 'business', 'education', 'assetacquisition', 'agriculture', 'other'],
        required: true
    },
    'otherPurposeDescription': 'string: min=2, max=100',
    'salutation': {
        '@type': 'string',
        valid: [
            'Mr', 'Mrs', 'Ms'
        ],
        required: true
    },
    'maritalStatus': {
        '@type': 'string',
        valid: [
            'single', 'married'
        ],
        required: true
    },
    'firstName': 'string: min=1, max=100, required',
    'middleName': 'string: min=1, max=100',
    'lastName': 'string: min=1, max=100, required',
    'personalEmail': 'email: min=2, max=100, required',
    'personalMobile': {
        '@type': 'string',
        'regex': /^[6-9][0-9]{9}$/,
        required: true
    },
    'isMobileVerified': 'boolean: required',
    'mobileVerificationTime': 'string: min=2, max=100',
    'mobileVerificationNotes': 'string: min=2, max=100',
    'employerName': 'string: min=2, max=100, required',
    'cinNumber': 'string: min=2, max=100, required',
    'workEmail': 'email: max=100, required',
    'income': 'number: min=0, max=1000000, required',
    'residenceType': {
        '@type': 'string',
        valid: ['OwnedSelf', 'Owned-Family', 'Rented-Family', 'Rented-Alone', 'Rented-Friends', 'PG-Hostel', 'Company-Acco'],
        required: true
    },
    'monthlyRent': 'number: min=0, max=1000000',
    'homeLoanEmi': 'number: min=0, max=1000000',

    'dateOfBirth': 'string: min=0, max=100, required',
    'aadhaarNumber': 'string: min=12, max=12, required',
    'permanentAccountNumber': 'string: min=10, max=10, required',

    'currentAddressLine1': 'string: min=3, max=100, required',
    'currentAddressLine2': 'string: min=0, max=100',
    'currentCity': 'string: min=3, max=100, required',
    'currentLocation': 'string: min=3, max=100, required',
    'currentPinCode': 'string: min=6, max=6, required',
    'currentState': 'string: min=3, max=100, required',

    'permanentAddressLine1': 'string: min=3, max=100, required',
    'permanentAddressLine2': 'string: min=0, max=100',
    'permanentLocation': 'string: min=3, max=100, required',
    'permanentCity': 'string: min=3, max=100, required',
    'permanentPinCode': 'string: min=6, max=6, required',
    'permanentState': 'string: min=3, max=100, required',

    'heighestEducation': {
        '@type': 'string',
        valid: ['highschool', 'bachelordegree', 'masterdegree', 'doctratedegree'],
        required: true
    },
    'educationalInstitution': 'string:min=2,max=100,required',

    'designation': 'string:min=2,max=100,required',
    'workExperience': 'number:min=0,max=10,required',

    'workAddressLine1': 'string: min=3, max=100, required',
    'workAddressLine2': 'string: min=0, max=100',
    'workLocation': 'string: min=3, max=100, required',
    'workCity': 'string: min=3, max=100, required',
    'workPinCode': 'string: min=6, max=6, required',
    'workState': 'string: min=3, max=100, required',

    'trackingCode': 'string: min=1,max=250',
    'trackingCodeMedium': 'string: min=1,max=250',
    'sodexoCode': 'string: min=1,max=250',

    'sourceReferenceId': 'string: min=0, max=100, required',
    'sourceType': 'string: min=0, max=100, required',
    'systemChannel': 'string: min=0, max=100, required',
    'currentAddressYear': 'string:min=0 max=2',  
    'employementYear': 'string:min=0 max=2',
};

function* set({
    $set,
    $debug,
    $stages: {
        authorization: {
            identity: {
                'create-account': createAccount
            }
        },
        api: {
            application: {
                'simulate-mobile-no': simulateMobileNo
            }
        }
    },
    value,
    $api,
    $configuration
}) {

    try {
        const {
            'application-processor': applicationProcessorEndpoint,
            'application-filters': applicationFiltersEndpoint
        } = yield $configuration('endpoints');

        const formatDate = (dt) => {
            const [dd, mm, yyyy] = dt.split('/');
            return `${yyyy}-${mm}-${dd}`;
        }

        const duplicateApplicationResponse = yield $api.get(`${applicationFiltersEndpoint}/duplicateexistsdata/Pan/${value.permanentAccountNumber}`);
        const [duplicateApplication] = duplicateApplicationResponse.body && duplicateApplicationResponse.body.length ? duplicateApplicationResponse.body : [{}];

        if (duplicateApplication.applicationNumber) {
            throw {
                code: 400,
                message: 'Application with same permanent account number already exists'
            };
        }

        $debug('Simulating mobile number');
        const mobileSimulation = yield simulateMobileNo.$execute({ mobileNumber: value.personalMobile });
        $debug('mobile number simulated - ', mobileSimulation);
        $debug('Completed simulating mobile number');

        $debug('In create-application method');

        // create application payload.
        const applicationSubmitPayload = {
            purposeOfLoan: value.purposeOfLoan,
            OtherPurposeDescription: value.otherPurposeDescription,
            requestedAmount: value.requestedAmount,
            personalMobile: value.personalMobile,
            isMobileVerified: value.isMobileVerified || false,
            mobileVerificationTime: this.mobileVerificationTime || (new Date()).toISOString().substring(0, 10),
            firstName: value.firstName,
            middleName: value.middleName,
            lastName: value.lastName,
            personalEmail: value.personalEmail,
            // userId: newUser.id,
            gender: value.salutation === 'Mr'
                ? 'Male'
                : 'Female',
            salutation: value.salutation,
            maritalStatus: value.maritalStatus,
            sourceReferenceId: value.sourceReferenceId || 'CRM',
            sourceType: value.sourceType || 'CRM',
            systemChannel: value.systemChannel || 'CRM',
            trackingCode: value.trackingCode || 'CRM',
            trackingCodeMedium: value.trackingCodeMedium || 'CRM',
            paymentFrequency: 'Monthly',
            requestedTermType: 'Monthly',
            requestedTermValue: '12.5',
            employmentStatus: 'Salaried',
            currentAddressYear: value.currentAddressYear,        
            employementYear: value.employementYear,            
        };

        console.log('---------------------------------------------------------------------');
        console.log('sourceReferenceId: ', value.sourceReferenceId || 'CRM',
            'sourceType: ', value.sourceType || 'CRM',
            'systemChannel: ', value.systemChannel || 'CRM',
            'trackingCode: ', value.trackingCode || 'CRM',
            'trackingCodeMedium: ', value.trackingCodeMedium || 'CRM');
        console.log('---------------------------------------------------------------------');

        $debug(`calling - ${applicationProcessorEndpoint}/application/submit`);
        const appResponse = yield $api.post(`${applicationProcessorEndpoint}/application/submit`, applicationSubmitPayload);
        const parsedApplicationObject = Object.assign(applicationSubmitPayload, {
            applicationNumber: appResponse.body.applicationNumber,
            applicantId: appResponse.body.applicantId
        });
        $debug(`completed calling - ${applicationProcessorEndpoint}/application/submit`);

        $debug('creating user');
        const randomPassword = Math.floor(100000 + Math.random() * 900000);
        $debug(`random number is ${randomPassword}`);

        const newUser = yield createAccount.$execute({
            username: value.personalMobile,
            password: randomPassword.toString(),
            name: `${value.firstName} ${value.lastName}`,
            email: value.personalEmail
        });
        yield $set('user', Object.assign({}, newUser, { randomNumber: randomPassword }));
        $debug('User created successfully.');

        const applicationNumber = appResponse.body.applicationNumber;

        // update-application payload.
        const appPayload = {
            applicationNumber,

            purposeOfLoan: value.purposeOfLoan,
            OtherPurposeDescription: value.otherPurposeDescription,
            requestedAmount: value.requestedAmount,
            requestedTermType: 'Monthly',
            requestedTermValue: '12.5',
            aadhaarNumber: value.aadhaarNumber,
            dateOfBirth: formatDate(value.dateOfBirth),
            salutation: value.salutation,
            firstName: value.firstName,
            middleName: value.middleName,
            lastName: value.lastName,
            gender: value.salutation === 'Mr' ? 'Male' : 'Female',
            maritalStatus: value.maritalStatus,
            userName: '', //username,
            password: '!@#123qwe',
            userId: newUser.id,
            permanentAccountNumber: value.permanentAccountNumber,
            personalMobile: value.personalMobile,
            isMobileVerified: value.isMobileVerified || false,
            mobileVerificationTime: this.mobileVerificationTime || (new Date()).toISOString().substring(0, 10),
            mobileVerificationNotes: value.mobileVerificationNotes || 'Testing',
            personalEmail: value.personalEmail,
            residenceType: value.residenceType,

            educationalInstitution: value.heighestEducation,
            levelOfEducation: value.heighestEducation,

            currentAddressLine1: value.currentAddressLine1,
            currentAddressLine2: value.currentAddressLine2,
            currentAddressLine3: null,
            currentAddressLine4: null,
            currentCity: value.currentCity,
            currentCountry: 'India',
            currentLandMark: null,
            currentLocation: value.currentLocation,
            currentPinCode: value.currentPinCode,
            currentState: value.currentState,

            permanentAddressLine1: value.permanentAddressLine1,
            permanentAddressLine2: value.permanentAddressLine2,
            permanentAddressLine3: null,
            permanentAddressLine4: null,
            permanentCity: value.permanentCity,
            permanentCountry: 'India',
            permanenttLandMark: null,
            permanentLocation: value.permanentLocation,
            permanentPinCode: value.permanentPinCode,
            permanentState: value.permanentState,

            employmentAsOfDate: null,
            cinNumber: value.cinNumber,
            employmentStatus: 'Salaried',
            employerName: value.employerName,
            workEmail: value.workEmail,
            income: value.income,
            paymentFrequency: 'Monthly',

            designation: value.designation,
            lengthOfEmploymentInMonths: value.workExperience * 12,
            workAddressLine1: value.workAddressLine1,
            workAddressLine2: value.workAddressLine2,
            workAddressLine3: null,
            workAddressLine4: null,
            workCity: value.workCity,
            workCountry: "India",
            workLandMark: null,
            workLocation: value.workLocation,
            workPinCode: value.workPinCode,
            workState: value.workState,
            debtPayments: (value.homeLoanEmi || 0),
            monthlyExpenses: 0, // monthlyExpenses        
            monthlyRent: value.monthlyRent,
            sourceReferenceId: value.sourceReferenceId || 'CRM',
            sourceType: value.sourceType || 'CRM',
            systemChannel: value.systemChannel || 'CRM',
            trackingCode: value.trackingCode || 'CRM',
            trackingCodeMedium: value.trackingCodeMedium || 'CRM',
            sodexoCode: value.sodexoCode,
            currentAddressYear: value.currentAddressYear,           
            employementYear: value.employementYear,          
        }

        console.log('update application payload ', appPayload);

        $debug(`calling - ${applicationProcessorEndpoint}/application/${applicationNumber}/update-application`);
        const appUpdateResponse = yield $api.put(`${applicationProcessorEndpoint}/application/${applicationNumber}/update-application`, appPayload);
        const returnValue = {
            type: appUpdateResponse.status.code === 202 ?
                'lead' :
                'eligible',
            application: appUpdateResponse.body
        };
        console.log('update application response ', returnValue);
        $debug(`completed calling - ${applicationProcessorEndpoint}/application/${applicationNumber}/update-application`);

        $debug('updating educatinal information');
        const eduResult = yield $api.put(`${applicationProcessorEndpoint}/application/${applicationNumber}/educationInformation`, {
            "LevelOfEducation": value.heighestEducation,
            "EducationalInstitution": value.educationalInstitution,
        });
        $debug('completed updating educatinal information');

        let initialOffer, initialOfferStatus;

        const offerResponse = yield $api.post(`${applicationProcessorEndpoint}/offer/${applicationNumber}/initialoffer`);
        if (!offerResponse.body.offers) {
            $debug('re-generated offer (retry from catch)', offerResponse.body);

            initialOfferStatus = {
                status: offerResponse.body.status || 'Rejected'
            };
        } else {
            $debug('returning from generateoffer true');

            const [offer] = offerResponse.body.offers;

            initialOffer = offer;
        }

        const response = Object.assign({}, returnValue.application, initialOffer, initialOfferStatus);

        return response;

    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];