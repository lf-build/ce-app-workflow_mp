const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};

function* set({
    $set,
    value: {
        applicationNumber
    },
    $api,
    $configuration,
    $debug,
}) {

    try {
        const {
            'saved-filters': savedFilters
        } = yield $configuration('endpoints');

        const filterPayload = {
            "searchFilterView": [{
                "ApplicationNumber": `${applicationNumber}`
            }],
            "operator": "and"
        };

        const response = yield $api.post(`${savedFilters}/application/search-applications`, filterPayload);

        let flowName = '';
        if (response.body && response.body.length > 0) {
            if (response.body[0].StatusWorkFlowId && response.body[0].StatusWorkFlowId != '' && response.body[0].StatusWorkFlowId != undefined && response.body[0].StatusWorkFlowId != null) {
                flowName = response.body[0].StatusWorkFlowId;
            }
        }

        return flowName;
    } catch (e) {
        $debug(e);

        if (e.status.code === 404) {
            throw {
                message: 'Application not found',
                code: 404,
            }
        }

        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];