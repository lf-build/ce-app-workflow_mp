const schema = {    
    applicationNumber: 'string:min=2,max=100,required',
    crmId: 'string:min=2,max=100,required',
};


function* set({
    value: {        
        applicationNumber,
        crmId
    },
    $api,
    $debug,
    $configuration,
}) {
    
    try {
        const {
            application: applicationEndpoint
        } = yield $configuration('endpoints');
        
        const response = yield $api.put(`${applicationEndpoint}/leadSquaredCRMId/${applicationNumber}/${crmId}`);
        return response && response.body;

    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];