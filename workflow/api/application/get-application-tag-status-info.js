const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};


function* set({
    value: {
        applicationNumber
    },
    $api,
    $debug,
    $configuration,
    $stages: {
        api: {
            application: {
                'get-application-details': getApplicationDetails,
                'get-application-tag-info': getApplicationTagInfo
            }
        }
    }
}) {
    try {
        const {
            'status-management': statusManagementEndpoint
        } = yield $configuration('endpoints');

        const applicationDetails = yield getApplicationDetails.$execute({
            applicationNumber
        });

        const applicationTagDetails = yield getApplicationTagInfo.$execute({
            applicationNumber
        });

        const statuManagemetnResponse = yield $api.get(`${statusManagementEndpoint}/application/${applicationNumber}`);

        if (applicationTagDetails && applicationDetails && statuManagemetnResponse && statuManagemetnResponse.body) {
            const {
                name,
                code,
                reasonsSelected,
                activeOn
            } = statuManagemetnResponse.body;

            const {
                tags
            } = applicationTagDetails;


            return {
                application: applicationDetails,
                StatusDetails: {
                    StatusName: name,
                    StatusCode: code,
                    ChangeReasons: reasonsSelected,
                    StatusChangedOn: activeOn.time || '',
                },
                Tags: tags

            }
        }

        // in case no response found from the above endpoints.
        throw {
            code: 202,
            message: 'Status and Tag related information not found'
        }

    }
    catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];