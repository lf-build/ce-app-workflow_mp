const {
    dbAction
} = require('@sigma-infosolutions/orbit-base/storage');

const schema = {
    mobileNumber: {
        '@type': 'string',
        'regex': /^[6-9][0-9]{9}$/,
        required: true
    }
};

function* set({
    $set,
    value: {
        mobileNumber
    },
    $debug,
}) {

    try {
        $debug(`Fetching workflow Id base on mobile number: ${mobileNumber}`);
        const readAllStates = (db) => db
            .collection('store')
            .find({
                'on-boarding.sign-up.mobile.mobileNumber': mobileNumber
            })
            .sort({
                _id: -1
            })
            .toArray();
        let states = yield dbAction(readAllStates);

        if (states.length === 0) {
            throw {
                code: 404,
                message: `No application found with mobile number ${mobileNumber}`
            }
        }

        let [persistedState] = states;
        console.log('Associated workflow Id ', persistedState._id);

        const {
            'sign-up': signUp
        } = persistedState['on-boarding'];

        const {            
            application: {
                applicationNumber
            }
        } = signUp;

        return {
            workflowId: persistedState._id,
            mobileNumber,
            applicationNumber
        }
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];