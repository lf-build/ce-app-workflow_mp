const schema = {
    applicationNumber: 'string:min=2,max=100,required',
    documentId: 'string:min=2,max=100,required',
};

/*
    TODO:
    1. adding the pipe logic here to get pdf.
 */

function* set({
    $set,
    value: {
        applicationNumber,
        documentId
    },
    $api,
    $configuration,
    $debug
}) {

    const {
        'application-document': applicationDocumentEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${applicationDocumentEndpoint}/application/${applicationNumber}/${documentId}/downloaddocument`);
        return response && response.body && response.body.downloadString;
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;
        throw {workflow/api/document/get-all-documents.js 
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];