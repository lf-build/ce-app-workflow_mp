const schema = {
    applicationNumber: 'string:min=2,max=100,required',
    category: 'string:min=2,max=100,required',
};

function* set({
    $set,
    value: {
        applicationNumber,
        category
    },
    $api,
    $configuration,
    $debug
}) {

    const {
        'application-document': applicationDocumentEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${applicationDocumentEndpoint}/${applicationNumber}/${category}`);
        return response && response.body;
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;
        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];