const schema = {
    applicationNumber: 'string:min=2,max=100,required'
};

/*
    TODO:
    1. need to check what all documents are required.
 */

function* set({
    $set,
    value: {
        applicationNumber
    },
    $api,
    $configuration,
    $debug,
    $stages: {
        api: {
            application: {
                'get-status-workflow-id': getStatusWorkflowId
            }
        }
    }
}) {

    const {
        'verification-engine': verificationEngineEndpoint
    } = yield $configuration('endpoints');

    try {

        // getting status workflow id as it is needed as part of multiproduct.
        const statusWorkflowId = yield getStatusWorkflowId.$execute({
            applicationNumber
        });
        const response = yield $api.get(`${verificationEngineEndpoint}/application/${applicationNumber}/${statusWorkflowId}/documents`);
        return response && response.body;
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;
        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];