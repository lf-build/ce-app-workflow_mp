const schema = {};


function* set({
    value,
    $api,
    $debug,
    $configuration,
}) {

    const {
        configuration: configurationEndpoint
    } = yield $configuration('endpoints');

    try {
        const response = yield $api.get(`${configurationEndpoint}/application-documents`);
        return response && response.body;
    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;
        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];