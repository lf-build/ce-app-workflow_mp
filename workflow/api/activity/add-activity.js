const schema = {
    description: 'string:min=2,max=100,required',
    applicationNumber: 'string:min=2,max=100,required',
    title: 'string:min=2,max=5000,required',
};


function* set({
    value: {
        description,
        applicationNumber,
        title
    },
    $api,
    $debug,
    $configuration,
}) {
    
    try {
        const {
            eventhub: eventhubEndpoint
        } = yield $configuration('endpoints');
        const payload = {
            Description: description,
            ApplicationNumber: applicationNumber,
            Title: title
        };
        
        const response = yield $api.post(`${eventhubEndpoint}/ApplicationActivityLogged`, payload);
        return response && response.body;

    } catch (e) {
        $debug(e);
        const message = e.message || e.body.message || 'Something went wrong';
        const code = e.code || e.body.code || 400;

        throw {
            message: message,
            code,
        }
    }
};

module.exports = [schema, set];