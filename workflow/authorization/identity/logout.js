const schema = {};

function *set({$api, $configuration, $debug})  {
  const { identity: identityEndpoint } = yield $configuration('endpoints');
  try{
    const response = yield $api.get(`${identityEndpoint}/logout`);
    return response.body;
  } catch(e) {
    $debug(e);
    throw {
      message: 'Failed to logout',
      code: 401,
    }
  }
};

module.exports = [schema, set];
