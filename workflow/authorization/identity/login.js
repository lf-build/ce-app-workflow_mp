const schema = {
  username: 'string:min=2,max=100,required',
  password: 'string:min=2,max=100,required',
  realm: 'string:min=2,max=100',
};

function* set({ $api, $configuration, value: { username, password, realm }, $debug }) {
  const { 'orbit-identity': orbitIdentityEndpoint } = yield $configuration('endpoints');
  if (!realm) {
    realm = 'borrower-portal';
  }
  try {
    const response = yield $api.post(`${orbitIdentityEndpoint}`, {
      client: 'credit-exchange',
      username,
      password,
      realm,
      rememberMe: true
    });
    return Object.assign({ realm }, response.body);
  } catch (e) {
    $debug(e);
    throw {
      code: 401,
      "details": [
        {
          "path": "username",
          message: 'Invalid Username or Password'
        }
      ]
    }
  }
};

module.exports = [schema, set];
