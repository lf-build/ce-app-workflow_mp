FROM node:slim

WORKDIR /app
ADD /package.json /app/package.json
RUN printf "//`node -p \"require('url').parse(process.env.NPM_REGISTRY_URL || 'https://registry.npmjs.org').host\"`/:_authToken=a3ea701d-292a-44b5-a666-f4ecde63ed4a\nregistry=${NPM_REGISTRY_URL:-https://registry.npmjs.org}\n" >> ~/.npmrc
RUN npm install --only=production

ADD . /app

ENTRYPOINT npm start
